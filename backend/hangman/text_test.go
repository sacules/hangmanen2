package hangman

import "testing"

func TestReplaceLetter(t *testing.T) {
	var lettertest = []struct {
		name, blankName, letter, expectedName string
	}{
		// Individual letters
		{"Anathema", "________", "t", "___t____"},
		{"Anathema", "________", "A", "A_______"},
		{"Anathema", "________", "a", "__a____a"},
		{"Anathema", "________", "j", "________"},
		{"The 1975", "___ ____", "9", "___ _9__"},

		// Completing the word
		{"Anathema", "________", "t", "___t____"},
		{"Anathema", "___t____", "A", "A__t____"},
		{"Anathema", "A__t____", "a", "A_at___a"},
		{"Anathema", "A_at___a", "n", "Anat___a"},
		{"Anathema", "Anat___a", "e", "Anat_e_a"},
		{"Anathema", "Anat_e_a", "h", "Anathe_a"},
		{"Anathema", "Anathe_a", "m", "Anathema"},

		// Full entries
		{"Cosmo Sheldrake - The Much Much How How and I", "_____ _________ - ___ ____ ____ ___ ___ ___ _", "h", "_____ _h_______ - _h_ ___h ___h ___ ___ ___ _"},
		{"Cosmo Sheldrake - The Much Much How How and I", "_____ _h_______ - _h_ ___h ___h ___ ___ ___ _", "H", "_____ _h_______ - _h_ ___h ___h H__ H__ ___ _"},

		// Random
		{"Daniel Olsén", "______ _____", "e", "____e_ _____"},
		{"", "", "", ""},
		{"ö", "_", "ö", "ö"},
		{"Sigur Rós", "_____ ___", "ó", "_____ _ó_"},
		{"Burial - Archangel", "______ - _________", "-", "______ - _________"},
	}

	for _, test := range lettertest {
		var result = ReplaceLetter(test.name, test.blankName, test.letter)
		if result != test.expectedName {
			t.Error("expected", test.expectedName, "and got", result)
		}
	}
}

func TestReplaceWord(t *testing.T) {
	var wordtest = []struct {
		name, blankName, word, expectedName string
	}{
		// Words
		{"Burial", "______", "Burial", "Burial"},

		// Full entries
		{"Cosmo Sheldrake - The Much Much How How and I", "_____ _________ - ___ ____ ____ ___ ___ ___ _", "Sheldrake", "_____ Sheldrake - ___ ____ ____ ___ ___ ___ _"},
		{"Cosmo Sheldrake - The Much Much How How and I", "_____ _________ - ___ ____ ____ ___ ___ ___ _", "Much Much", "_____ _________ - ___ Much Much ___ ___ ___ _"},
		{"Cosmo Sheldrake - The Much Much How How and I", "_____ _________ - ___ ____ ____ ___ ___ ___ _", "Much Much", "_____ _________ - ___ Much Much ___ ___ ___ _"},
		{"Cosmo Sheldrake - The Much Much How How and I", "_____ _________ - ___ ____ ____ ___ ___ ___ _", "How", "_____ _________ - ___ ____ ____ How ___ ___ _"},
		{"Cosmo Sheldrake - The Much Much How How and I", "_____ _________ - ___ ____ ____ ___ ___ ___ _", "Guacamole", "_____ _________ - ___ ____ ____ ___ ___ ___ _"},
	}

	for _, test := range wordtest {
		var result = ReplaceWord(test.name, test.blankName, test.word)
		if result != test.expectedName {
			t.Error("expected", test.expectedName, "and got", result)
		}
	}
}

func TestCreateBlankName(t *testing.T) {
	var blanktest = []struct {
		name, expectedBlank string
	}{
		// Words
		{"Anathema", "________"},
		{"", ""},

		// Full entries
		{"Devin Townsend - Earth Day", "_____ ________ - _____ ___"},
		{"At the Drive-In - Two-Armed Scissor", "__ ___ _____-__ - ___-_____ _______"},

		// Special characters
		{"Exit Music (For a Film)", "____ _____ (___ _ ____)"},
		{"Closer [2017 Remaster]", "______ [____ ________]"},
	}

	for _, test := range blanktest {
		var result = CreateBlankName(test.name)
		if result != test.expectedBlank {
			t.Error("expected", test.expectedBlank, "and got", result)
		}
	}
}
