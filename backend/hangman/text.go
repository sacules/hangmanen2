package hangman

import (
	"strings"
	"unicode/utf8"
)

const (
	// SpecialChars collects characters that shouldn't be turned into '_'
	SpecialChars string = `"[]-(),.:;?!'& /_\`
)

func replaceAtIndex(blankName string, l rune, i int) string {
	newBlank := []rune(blankName)
	newBlank[i] = l
	return string(newBlank)
}

// ReplaceLetter inserts each occurence of the given letter in name on blankName.
func ReplaceLetter(name, blankName, letter string) string {
	for i, l := range name {
		if string(l) == letter {
			blankName = replaceAtIndex(blankName, l, i)
		}
	}
	return blankName
}

// ReplaceWord inserts the first occurence of the given word in name on blankName.
func ReplaceWord(name, blankName, word string) string {
	index := strings.Index(name, word)

	if index != -1 {
		for _, l := range word {
			blankName = replaceAtIndex(blankName, l, index)
			index++
		}
	}
	return blankName
}

// CreateBlankName generates a string in which each character is replaced
// with a '_', except for the SpecialChars.
func CreateBlankName(name string) string {
	blankName := strings.Repeat("_", utf8.RuneCountInString(name))

	for i, l := range name {
		specialIndex := strings.IndexRune(SpecialChars, l)
		if specialIndex == -1 {
			continue
		}

		char := rune(SpecialChars[specialIndex])
		blankName = replaceAtIndex(blankName, char, i)
	}

	return blankName
}
