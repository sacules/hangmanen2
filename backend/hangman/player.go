package hangman

// Player represents a user playing the game
type Player struct {
	Name    string `json:"name"`
	Guessed bool   `json:"guessed"`
}
