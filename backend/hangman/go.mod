module gitlab.com/sacules/hangmanen2/backend/hangman

go 1.13

require (
	github.com/DataDog/zstd v1.4.1 // indirect
	github.com/Sereal/Sereal v0.0.0-20190618215532-0b8ac451a863 // indirect
	github.com/asdine/storm v2.1.2+incompatible
	github.com/go-chi/chi v4.0.2+incompatible
	github.com/go-chi/cors v1.0.0
	github.com/go-chi/render v1.0.1
	github.com/golang/protobuf v1.3.2 // indirect
	github.com/golang/snappy v0.0.1 // indirect
	github.com/google/go-cmp v0.2.0
	github.com/logrusorgru/aurora v0.0.0-20191116043053-66b7ad493a23
	github.com/matryer/is v1.2.0
	github.com/pkg/errors v0.8.1
	github.com/stretchr/testify v1.4.0 // indirect
	github.com/vmihailenco/msgpack v4.0.4+incompatible // indirect
	gitlab.com/Sacules/jsonfile v0.2.2
	go.etcd.io/bbolt v1.3.3 // indirect
	golang.org/x/net v0.0.0-20190620200207-3b0461eec859 // indirect
	golang.org/x/sys v0.0.0-20191119060738-e882bf8e40c2 // indirect
	google.golang.org/appengine v1.6.5 // indirect
)
