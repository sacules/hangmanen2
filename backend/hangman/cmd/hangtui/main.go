package main

import (
	"bufio"
	"errors"
	"fmt"
	"math/rand"
	"os"
	"strings"
	"time"

	"gitlab.com/Sacules/jsonfile"
	"gitlab.com/sacules/hangmanen2/backend/hangman"
)

func read(prompt string) string {
	fmt.Print(prompt)

	s := bufio.NewScanner(os.Stdin)
	s.Scan()

	str := s.Text()
	if err := s.Err(); err != nil {
		fmt.Println("error reading:", err)
	}

	return str
}

func addTracks() []*hangman.Entry {
	tracks := make([]*hangman.Entry, 0)

	for {
		player := read("> player: ")
		track := read("> track: ")

		entry := hangman.NewEntry(track, player)
		tracks = append(tracks, entry)

		fmt.Println()
		newsong := read("> enter a new song? [Y/n]: ")
		if newsong == "n" {
			break
		}
	}

	return tracks
}

func printHangman(tracks []*hangman.Entry) {
	for _, t := range tracks {
		if t.Player.Guessed {
			fmt.Printf("%s: %s\n", t.Player.Name, t.BlankTitle)
		} else {
			fmt.Printf("?: %s\n", t.BlankTitle)
		}
	}
}

func shuffle(tracks []*hangman.Entry) {
	rand.Seed(time.Now().UnixNano())
	rand.Shuffle(len(tracks), func(i, j int) {
		tracks[i], tracks[j] = tracks[j], tracks[i]
	})
}

func guessLetter(tracks []*hangman.Entry, letter string) {
	for i := range tracks {
		tracks[i].GuessLetter(letter)
		tracks[i].GuessLetter(strings.ToUpper(letter))
	}
}

func guessPlayer(tracks []*hangman.Entry, p string) {
	for i := range tracks {
		if tracks[i].Player.Name == p {
			tracks[i].Player.Guessed = true
		}
	}
}

func guessWord(tracks []*hangman.Entry, w string) {
	for i := range tracks {
		tracks[i].GuessWord(w)
	}
}

func printLetters(letters []string) {
	fmt.Println()
	fmt.Print("Guessed letters: ")
	for _, l := range letters {
		fmt.Print(l, "")
	}

	fmt.Println()
	fmt.Println()
}

func printComplete(tracks []*hangman.Entry) {
	for _, t := range tracks {
		fmt.Printf("%s: %s\n", t.Player.Name, t.Title)
	}

	fmt.Println()
}

func main() {
	words := make([]string, 0)
	err := jsonfile.Load(&words, "words.json")
	if !errors.Is(err, os.ErrNotExist) && err != nil {
		fmt.Fprintf(os.Stderr, "error while loading words: %v", err)
		os.Exit(1)
	}
	letters := make([]string, 0)
	err = jsonfile.Load(&letters, "letters.json")
	if !errors.Is(err, os.ErrNotExist) && err != nil {
		fmt.Fprintf(os.Stderr, "error while loading letters: %v", err)
		os.Exit(1)
	}

	tracks := make([]*hangman.Entry, 0)
	err = jsonfile.Load(&tracks, "entries.json")
	if !errors.Is(err, os.ErrNotExist) && err != nil {
		fmt.Fprintf(os.Stderr, "error while loading tracks: %v", err)
		os.Exit(1)
	}

	// recompute the blank titles
	for i, t := range tracks {
		tracks[i].BlankTitle = hangman.CreateBlankName(t.Title)
	}

	for _, l := range letters {
		guessLetter(tracks, l)
	}

	for _, w := range words {
		guessWord(tracks, w)
	}

	for {
		printHangman(tracks)
		printLetters(letters)
		in := read("> (a) add a track (l) guess a letter (w) guess a word (p) guess a player (s) shuffle (c) show complete hangman (q) quit: ")

		switch in {
		case "a":
			tracks = append(tracks, addTracks()...)

		case "c":
			printComplete(tracks)

		case "l":
			l := read("> letter to guess: ")
			guessLetter(tracks, l)
			letters = append(letters, l)

		case "w":
			w := read("> word to guess: ")
			guessWord(tracks, w)
			words = append(words, w)

		case "p":
			p := read("> player to guess: ")
			guessPlayer(tracks, p)

		case "s":
			shuffle(tracks)

		case "q":
			return
		}

		err := jsonfile.Save(tracks, "entries.json")
		if err != nil {
			fmt.Fprintf(os.Stderr, "error while saving: %v", err)
			os.Exit(1)
		}

		err = jsonfile.Save(letters, "letters.json")
		if err != nil {
			fmt.Fprintf(os.Stderr, "error while saving: %v", err)
			os.Exit(1)
		}

		err = jsonfile.Save(words, "words.json")
		if err != nil {
			fmt.Fprintf(os.Stderr, "error while saving: %v", err)
			os.Exit(1)
		}
	}
}
