package main

import (
	"gitlab.com/sacules/hangmanen2/backend/hangman"
)

type entryModel struct {
	hangman.Entry `storm:"unique"`

	ID int `storm:"increment"`
}

type epModel struct {
	hangman.EP `storm:"unique"`

	ID int `storm:"increment"`
}
