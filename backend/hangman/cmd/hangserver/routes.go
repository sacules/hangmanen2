package main

import (
	"fmt"
	"log"
	"net/http"
	"strings"

	"github.com/asdine/storm"
	"github.com/go-chi/chi"
	"github.com/go-chi/render"

	"gitlab.com/sacules/hangmanen2/backend/hangman"
)

// makes sense?
type guesser interface {
	guessLetter()
	guessWord()
}

func (s *server) routes() {
	s.router.Mount("/", http.FileServer(http.Dir("./public")))

	s.router.Route("/api", func(r chi.Router) {
		// root
		r.Get("/", s.handleGameGet())
		r.Post("/", s.handleGameCreate())
		r.Delete("/", s.handleGameDelete())

		// entries
		r.Route("/entries", func(r chi.Router) {
			r.Get("/", s.handleEntriesGet())
			r.Post("/", s.handleEntriesCreate())
			// r.Patch("/", s.createEntries())
			// r.Delete("/", s.createEntries())
		})

		// eps
		r.Route("/eps", func(r chi.Router) {
			// r.Get("/", getEntries)
			// r.Post("/", s.handleEntriesCreate())
			// r.Patch("/", s.createEntries())
			// r.Delete("/", s.createEntries())
		})

		// guessing
		r.Route("/guess", func(r chi.Router) {
			r.Get("/", s.handleGuessesGet())
			r.Post("/letter", s.handleGuessLetter())
			// r.Post("/word", s.handleEntryGuessWord())
		})
	})

	log.Println("installed the following routes:")
	walkFunc := func(method, route string, handler http.Handler, middlewares ...func(http.Handler) http.Handler) error {
		route = strings.Replace(route, "/*/", "/", -1)
		log.Printf("%s\t%s", au.Bold(method), route)
		return nil
	}

	chi.Walk(s.router, walkFunc)
}

func (s *server) handleGameGet() http.HandlerFunc {
	type respGame struct {
		Category string `json:"category"`
	}

	return func(w http.ResponseWriter, r *http.Request) {
		cat, err := s.dbGetCategory()
		if err != nil {
			log.Println("handle game get:", err)
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		resp := respGame{cat}

		render.JSON(w, r, &resp)
	}
}

func (s *server) handleGameCreate() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		cat, err := s.dbGetCategory()
		if err != nil {
			log.Println("handle game get:", err)
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		if cat == hangman.GameEntry || cat == hangman.GameEP {
			http.Error(w, "game already running, please delete first", http.StatusConflict)
			return
		}

		if err := r.ParseForm(); err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}

		newcat := r.PostFormValue("category")
		if newcat != hangman.GameEntry && newcat != hangman.GameEP {
			http.Error(w, fmt.Sprintf("expected either '%s' or '%s' categories, got '%s'", hangman.GameEntry, hangman.GameEP, newcat), http.StatusBadRequest)
			return
		}

		cat = newcat
		err = s.dbSetCategory(cat)
		if err != nil {
			log.Println("error saving game category:", err)
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		log.Printf("handle game create: changed game category to '%s'", newcat)
		w.WriteHeader(http.StatusCreated)
	}
}

func (s *server) handleGameDelete() http.HandlerFunc {
	// TODO: delete everything - db, guesses, entries, etc
	return func(w http.ResponseWriter, r *http.Request) {
		cat, err := s.dbGetCategory()
		if cat == "" {
			http.Error(w, "game already deleted", http.StatusNotModified)
			return
		}

		if err != nil {
			log.Println("handle game delete:", err)
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		err = s.dbDeleteCategory()
		if err != nil {
			log.Println("handle game delete:", err)
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
	}
}

func (s *server) handleEntriesGet() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var entries []entryModel

		err := s.db.All(&entries)
		if err != nil {
			log.Println("get entries:", err)
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		render.JSON(w, r, &entries)
	}
}

func (s *server) handleEntriesCreate() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		cat, err := s.dbGetCategory()
		if err != nil {
			log.Println("create entries:", err)
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		if cat == "" {
			http.Error(w, "set up a category first", http.StatusBadRequest)
			return
		}

		if cat != hangman.GameEntry {
			http.Error(w, "can't create an entry while the game is "+cat, http.StatusBadRequest)
			return
		}

		err = r.ParseForm()
		if err != nil {
			log.Println("create entries:", err)
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		playerName := r.FormValue("name")
		title := r.FormValue("title")
		if playerName == "" || title == "" {
			http.Error(w, "invalid entry", http.StatusBadRequest)
			return
		}

		err = s.dbCreateEntry(playerName, title)
		if err != nil {
			log.Println("create entries:", err)
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		log.Println("created new entry:", title, "by", playerName)
		w.WriteHeader(http.StatusCreated)
	}
}

func (s *server) handleGuessesGet() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		guesses, err := s.dbGetGuesses()
		switch {
		case err == storm.ErrNotFound:
			http.Error(w, "setup the guesses first", http.StatusBadRequest)
			return

		case err != nil:
			log.Println("handle get guesses:", err)
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		render.JSON(w, r, &guesses)
	}
}

func (s *server) handleGuessLetter() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		guesses, err := s.dbGetGuesses()
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			log.Println("guess entries:", err)
			return
		}

		err = r.ParseForm()
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}

		l := strings.ToLower(r.FormValue("letter"))
		player := r.FormValue("player")
		switch {
		case l == "" || len(l) > 1:
			http.Error(w, "didn't receive a letter", http.StatusBadRequest)
			return

		case player == "":
			http.Error(w, "invalid player", http.StatusBadRequest)
			return

			// case guesses.RepeatedLetter(l):
			// 	// TODO: change all the errors of input to a standard json response
			// 	http.Error(w, "letter already guessed", http.StatusBadRequest)
			// 	return
		}

		cat, err := s.dbGetCategory()
		if err != nil {
			log.Println("handle guess letter:", err)
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		if cat == hangman.GameEP {
			return
		}

		var entries []entryModel
		err = s.db.All(&entries)
		if err != nil {
			log.Println("guess letter:", err)
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		if len(entries) == 0 {
			http.Error(w, "nothing to guess", http.StatusBadRequest)
			return
		}

		log.Println("guessing letter:", l)
		for _, e := range entries {
			e.GuessLetter(l)
			e.GuessLetter(strings.ToUpper(l))

			err = s.db.Update(&e)
			if err != nil {
				log.Println("guess letter:", err)
				http.Error(w, "couldn't save entries", http.StatusInternalServerError)
				return
			}
		}

		// TODO: Check that player is not in the recent guesses list
		hangplayer := hangman.Player{Name: player, Guessed: false}

		guesses.AddPlayer(hangplayer)
		guesses.AddLetter(l)

		err = s.dbSetGuesses(guesses)
		if err != nil {
			log.Fatal("guess letter:", err)
			http.Error(w, err.Error(), http.StatusInternalServerError)
		}
	}
}

// func (s *server) handleEntryGuessWord() http.HandlerFunc {
// 	// Create a new bucket if it doesn't exist
// 	err := s.dbGetGuesses()
// 	if err != nil {
// 		log.Fatal("guess entries:", err)
// 	}

// 	return func(w http.ResponseWriter, r *http.Request) {
// 		err = r.ParseForm()
// 		if err != nil {
// 			http.Error(w, err.Error(), http.StatusBadRequest)
// 			return
// 		}

// 		word := r.FormValue("word")
// 		if word == "" || len(word) <= 1 {
// 			http.Error(w, "didn't receive a valid word", http.StatusBadRequest)
// 			return
// 		}

// 		err := s.dbGetGuesses()
// 		if err != nil {
// 			http.Error(w, err.Error(), http.StatusInternalServerError)
// 			log.Fatal("guess entries:", err)
// 		}

// 		if s.guessesModel.RepeatedWord(word) {
// 			http.Error(w, "word already guessed", http.StatusBadRequest)
// 			return
// 		}

// 		log.Println("guessing word:", word)

// 		var entries []entryModel
// 		err = s.db.All(&entries)
// 		if err != nil {
// 			log.Println("get entries:", err)
// 			http.Error(w, err.Error(), http.StatusInternalServerError)
// 			return
// 		}

// 		for _, e := range entries {
// 			e.GuessWord(word)

// 			err = s.db.Update(&e)
// 			if err != nil {
// 				log.Println("create entries:", err)
// 				http.Error(w, "couldn't save entries", http.StatusInternalServerError)
// 				return
// 			}
// 		}

// 		s.guessesModel.AddWord(word)
// 		err = s.db.Set("game state", "guesses", &s.guessesModel)
// 		if err != nil {
// 			log.Fatal("guess entries:", err)
// 		}
// 	}
// }
